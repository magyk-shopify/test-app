import React, { useEffect } from 'react';
import { createApp } from '@shopify/app-bridge';
import { Provider, TitleBar } from '@shopify/app-bridge-react';
import { shopifyConfig } from '../shopifyConfig';
import { json } from "@remix-run/node";
import { Link, Outlet, useLoaderData, useRouteError } from "@remix-run/react";
import { boundary } from "@shopify/shopify-app-remix/server";
import { AppProvider } from "@shopify/shopify-app-remix/react";
import { NavMenu } from "@shopify/app-bridge-react";
import polarisStyles from "@shopify/polaris/build/esm/styles.css?url";
import { authenticate } from "../shopify.server";

export const links = () => [{ rel: "stylesheet", href: polarisStyles }];

export const loader = async ({ request }) => {
  await authenticate.admin(request);

  return json({ apiKey: process.env.SHOPIFY_API_KEY || "" });
};

export default function App() {
    const { apiKey } = useLoaderData();

    useEffect(() => {
        const appBridge = createApp(shopifyConfig);
        TitleBar.create(appBridge, { title: 'My Shopify App' });
    }, []);

    return (
        <html>
        <head>
            <title>Shopify App</title>
        </head>
        <body>
            <Provider config={shopifyConfig}>
            {
                <AppProvider isEmbeddedApp apiKey={apiKey}>
                    <NavMenu>
                    <   Link to="/app" rel="home">
                        Home
                    </Link>
                    <Link to="/app/additional">Additional page</Link>
                    </NavMenu>
                    <Outlet />
                </AppProvider>
                }
            </Provider>
        </body>
        </html>
    );
}
