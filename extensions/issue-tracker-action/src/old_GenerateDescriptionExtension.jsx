import React, { useCallback, useEffect, useState } from "react";
import {
  reactExtension,
  useApi,
  AdminAction,
  Button,
  Box,
  Banner,
  TextArea,
  Select,
  Text,
} from "@shopify/ui-extensions-react/admin";
import { generateProductDescription, getDescription, updateDescription } from "./utils";

const TARGET = "admin.product-details.action.render";

export default reactExtension(TARGET, () => <App />);

function App() {
  const { close, data } = useApi(TARGET);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [ageRange, setAgeRange] = useState("");
  const [gender, setGender] = useState("");
  const [country, setCountry] = useState("");
  const [positioning, setPositioning] = useState("");

  const fetchProductDetails = useCallback(async () => {
    setLoading(true);
    setError(null);
    try {
      console.log("API Data:", data);

      const productId = data?.selected?.[0]?.id;
      if (!productId) {
        throw new Error("Product ID is missing.");
      }
      console.log("Product ID:", productId);

      const productDetails = await getDescription(productId);
      console.log("Product Details:", productDetails);

      const productTitle = productDetails.title;
      if (!productTitle) {
        throw new Error("Product title is missing.");
      }
      setTitle(productTitle);

      const generatedDescription = await generateProductDescription(productTitle);
      if (generatedDescription) {
        setDescription(generatedDescription);
      } else {
        setError("Failed to generate description. The response was empty.");
      }
    } catch (err) {
      if (err.message.includes("429")) {
        setError("You have exceeded your current API quota.");
      } else {
        console.error("Error fetching product details or generating description:", err);
        setError(`An error occurred: ${err.message}. Please try again.`);
      }
    } finally {
      setLoading(false);
    }
  }, [data]);

  const handleUpdateDescription = async () => {
    try {
      setLoading(true);
      setError(null);
      const productId = data?.selected?.[0]?.id;
      if (!productId) {
        throw new Error("Product ID is missing.");
      }
      const generatedDescription = await generateProductDescription(title);
      if (!generatedDescription) {
        setError("Failed to generate description. The response was empty.");
        return;
      }
      await updateDescription(productId, generatedDescription);
      setDescription("Description updated successfully.");
    } catch (err) {
      console.error("Error updating product description:", err);
      setError(`An error occurred: ${err.message}. Please try again.`);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchProductDetails();
  }, [fetchProductDetails]);

  return (
    <AdminAction
      title="Generate Description"
      primaryAction={<Button onPress={handleUpdateDescription}>Update</Button>}
    >
      <Box paddingBlockStart="large">
        <Text as="h3">Tell me about the target customer for this product</Text>
        <Select
          label="Age range"
          options={[
            { label: "18-21", value: "18-21" },
            { label: "21-25", value: "21-25" },
            { label: "25-35", value: "25-35" },
            { label: "35-50", value: "35-50" },
            { label: "50+", value: "50+" }
          ]}
          value={ageRange}
          onChange={(value) => setAgeRange(value)}
        />
        <Select
          label="Gender"
          options={[
            { label: "Male", value: "Male" },
            { label: "Female", value: "Female" },
            { label: "All", value: "All" }
          ]}
          value={gender}
          onChange={(value) => setGender(value)}
        />
        <Select
          label="Country or Location"
          options={[
            { label: "United States", value: "US" },
            { label: "Canada", value: "CA" },
            { label: "United Kingdom", value: "UK" },
            { label: "Australia", value: "AU" },
            // Other countries to be added
          ]}
          value={country}
          onChange={(value) => setCountry(value)}
        />
        <Select
          label="Positioning"
          options={[
            { label: "Value", value: "Value" },
            { label: "Discount", value: "Discount" },
            { label: "Premium", value: "Premium" },
            { label: "Luxury", value: "Luxury" }
          ]}
          value={positioning}
          onChange={(value) => setPositioning(value)}
        />
      </Box>
      <Box paddingBlockStart="large">
        {loading ? (
          <Box>Loading...</Box>
        ) : (
          <TextArea
            value={description}
            label="Generated Description"
            readOnly
          />
        )}
        {error && (
          <Box paddingBlockStart="large">
            <Banner status="critical">{error}</Banner>
          </Box>
        )}
      </Box>
    </AdminAction>
  );
}
