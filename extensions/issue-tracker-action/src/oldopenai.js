import OpenAI from 'openai';

const openai = new OpenAI({
  apiKey: "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"
});

async function titleToDescription(title) {
  const params = {
    messages: [{ 'role': 'system', 'content': 'You are a designer, you describe product very beautifully. You grab all the fine details of the product and describe them in perfect words.' },
    { 'role': 'user', 'content': `Generate a product description for the following title, do not add any additional commentary: "${title}"` }
    ],
    model: 'gpt-3.5-turbo',
  }
  const response = await openai.chat.completions.create(params);
  console.log('Generated Product Description', JSON.stringify({ request: params, response }))
  return response.choices[0].message.content; 
}


export async function fetchDescriptionFromOpenAI(title) {
  console.log("Fetching description for title:", title);
  const description = await titleToDescription(title);

  if (!description) {
    console.error("Error fetching description from OpenAI");
    throw new Error(`Error fetching description`);
  }
  return description;
}


