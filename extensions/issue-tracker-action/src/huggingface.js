import fetch from 'node-fetch';

const apiKey = "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";

export async function fetchDescriptionFromHuggingFace(title) {
  const params = {
    inputs: title
  };

  const response = await fetch("https://api-inference.huggingface.co/models/facebook/bart-large-cnn", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${apiKey}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(params),
  });

  if (!response.ok) {
    console.error("Error fetching description from Hugging Face:", response.statusText);
    throw new Error(`Error fetching description: ${response.statusText}`);
  }

  const data = await response.json();
  console.log('Generated Product Description', JSON.stringify({ request: params, response: data }));

  if (!data || !data[0] || !data[0].summary_text) {
    throw new Error("Invalid response format");
  }

  return data[0].summary_text;
}

// export async function fetchDescriptionFromHuggingFace(title) {
//   console.log("Fetching description for title:", title);
//   const description = await titleToDescription(title);

//   if (!description) {
//     console.error("Error fetching description from Hugging Face");
//     throw new Error("Error fetching description");
//   }
//   return description;
// }
 

// import fetch from 'node-fetch';

// const apiKey = "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";

// async function titleToDescription(title) {
//   const params = {
//     inputs: `Describe the word: ${title}`
//   };

//   const response = await fetch("https://api-inference.huggingface.co/models/gpt-3", {
//     method: "POST",
//     headers: {
//       Authorization: `Bearer ${apiKey}`,
//       "Content-Type": "application/json",
//     },
//     body: JSON.stringify(params),
//   });

//   if (!response.ok) {
//     console.error("Error fetching description from Hugging Face:", response.statusText);
//     throw new Error(`Error fetching description: ${response.statusText}`);
//   }

//   const data = await response.json();
//   console.log('Generated title Description', JSON.stringify({ request: params, response: data }));

//   if (!data || !data.choices || !data.choices[0] || !data.choices[0].text) {
//     throw new Error("Invalid response format");
//   }

//   return data.choices[0].text.trim();
// }

// export async function fetchDescriptionFromHuggingFace(title) {
//   console.log("Fetching description for title:", title);
//   const description = await titleToDescription(title);

//   if (!description) {
//     console.error("Error fetching description from Hugging Face");
//     throw new Error("Error fetching description");
//   }
//   return description;
// }
