// utils.js

import { fetchDescriptionFromOpenAI } from "./openai";
import { fetchDescriptionFromHuggingFace } from "./huggingface";

export async function generateProductDescription(title) {
    return await fetchDescriptionFromOpenAI(title);
}

export async function generateProductDescription2(title) {
    return await fetchDescriptionFromHuggingFace(title);
}

export async function updateDescriptionWithGeneratedDescription(productId) {
  const productDetails = await getDescription(productId);
  const productTitle = productDetails.title;
  const generatedDescription = await generateProductDescription(productTitle);
  await updateDescription(productId, generatedDescription);
}

export async function getDescription(productId) {
    const response = await makeGraphQLQuery(
      `query Product($id: ID!) {
        product(id: $id) {
          title
          metafield(namespace: "$app:description", key: "description") {
            value
          }
        }
      }`,
      { id: productId }
    );
  
    // console.log("GraphQL response:", response);
  
    if (!response.data || !response.data.product) {
      throw new Error("Product data is missing.");
    }
  
    return response.data.product;
}
  
export async function updateDescription(productId, newDescription) {
    return await makeGraphQLQuery(
      `mutation SetMetafield($namespace: String!, $ownerId: ID!, $key: String!, $type: String!, $value: String!) {
        metafieldDefinitionCreate(
          definition: {namespace: $namespace, key: $key, name: "Generated Description", ownerType: PRODUCT, type: $type, access: {admin: MERCHANT_READ_WRITE}}
        ) {
          createdDefinition {
            id
          }
        }
        metafieldsSet(metafields: [{ownerId:$ownerId, namespace:$namespace, key:$key, type:$type, value:$value}]) {
          userErrors {
            field
            message
            code
          }
        }
      }`,
      {
        ownerId: productId,
        namespace: "$app:description",
        key: "description",
        type: "single_line_text_field",
        value: newDescription,
      }
    );
}

async function makeGraphQLQuery(query, variables) {
    const graphQLQuery = {
      query,
      variables,
    };
  
    const res = await fetch("shopify:admin/api/graphql.json", {
      method: "POST",
      body: JSON.stringify(graphQLQuery),
      headers: {
        'Content-Type': 'application/json',
      }
    });
  
    // console.log("Response status:", res.status); 
    if (!res.ok) {
      console.error("Network error:", res.status, res.statusText);
    }
  
    const jsonResponse = await res.json();
    // console.log("GraphQL Response:", jsonResponse);

    return jsonResponse;
}
